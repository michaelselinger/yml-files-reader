"""
Skript that takes a Yaml file from the GitLab CI and Simulates it in
a Docker Container.
Created by Michael Selinger
"""

import yaml
import docker
import requests


def main(file_url):
    """
    This is the main function that performs the following steps:
    - Downloads a YAML file from a given URL.
    - Loads the YAML data.
    - Retrieves the Docker image from the YAML data.
    - Creates a Docker container.
    - Executes scripts within the Docker container.
    - Stops and removes the Docker container.
    """

    # Download the YAML file from the URL
    data = load_yaml_from_url(file_url)

    # Get the image from the Yaml file
    image = get_image(data)
    print("Image:", image)

    # Create a Docker client object
    client = docker.from_env()

    # Pull the Docker image
    client.images.pull(image)

    # Create a Docker container
    container = client.containers.create(image, command='/bin/bash', tty=True,
                                         stdin_open=True, auto_remove=False,
                                         privileged=True)
    print(f"Container Status: {container.status}")

    # Start the Docker container
    container.start()

    # Get the Docker container name
    name = client.containers.list()[0].name
    print("Container Name: ", name)

    # Execute Scripts inside a Docker container
    scripts = extract_scripts_from_data(data)
    print("Scripts:", scripts)
    for command in scripts:
        print(command)
        exec_log, output = container.exec_run(["/bin/bash",
                                              command],
                                              stdout=True,
                                              stderr=True,
                                              stream=True)
        print(output.decode('utf-8'))
        for line in exec_log[1]:
            print(line, end='')

    print('Container Finished outputting log.')

    # Stop and remove the container
    container.stop()
    print('Container stopped')
    container.remove()
    print('Container removed')


def load_yaml_from_url(url):
    """
    Load Yml from url.
    """
    response = requests.get(url)
    data = yaml.safe_load(response.text)
    return data


def get_image(data):
    """
    This function retrieves the Docker image from the YAML data.
    """
    if isinstance(data, dict):
        for key in data:
            if key == "image":
                return data[key]
            elif isinstance(data[key], list):
                for item in data[key]:
                    image = get_image(item)
                    if image is not None:
                        return image
            elif isinstance(data[key], dict):
                image = get_image(data[key])
                if image is not None:
                    return image
    # Return 'default' if no 'image' key was found
    return "default"


def extract_scripts_from_data(data):
    """
    This function extracts all scripts from the YAML data.
    """
    scripts = []

    if isinstance(data, dict):
        for key in data:
            if key == "script":
                scripts.extend(data[key])
            elif key == 'before_script':
                scripts.extend(data[key])
            elif key == 'after_script':
                scripts.extend(data[key])
            elif isinstance(data[key], list):
                for item in data[key]:
                    scripts.extend(extract_scripts_from_data(item))
            elif isinstance(data[key], dict):
                scripts.extend(extract_scripts_from_data(data[key]))
            else:
                pass  # ignore other data types

    # Return an empty list if no scripts were found
    if not scripts:
        return []

    return scripts


if __name__ == "__main__":
    FILE_URL = "https://raw.githubusercontent.com/yiisoft/yii2/master\
                /.gitlab-ci.yml"
    main(FILE_URL)
